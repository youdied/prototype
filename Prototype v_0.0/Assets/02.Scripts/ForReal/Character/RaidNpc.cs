﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaidNpc : MonoBehaviour
{
    public int eventLevel = 0;
    public GameObject lv0Panel;

    public void Lv0EventActive()
    {
        UIManager.instance.NPCEvent0_Active();
    }
    public void Lv0EventDisactive()
    {
        UIManager.instance.NPCEvent0_Disactive();
    }
   
}
